from scipy import stats
import matplotlib.pyplot as plt
import pickle
import numpy as np

# INPUT: d1 : list of first set of data points
# 		 d2 : list of first set of data points
# OUTPUT: NULL
# PURPOSE: performs paired t test between d1 and d2
def paired_t_test(d1, d2):
	t_stat, p_val = stats.ttest_rel(d1,d2)
	print "T STATISTIC: "+str(t_stat)+ " with P-VALUE: "+str(p_val)

def graph_pdf(d):
	ax.plot(x, norm.pdf(x),
        'r-', lw=5, alpha=0.6, label='norm pdf')

# INPUT: d : list of data points
# 		 title: the title to be displayed on the plot (string)
# OUTPUT: NULL
# PURPOSE: uses the plt library to plot a histogram of the data set d
def plt_hist(d, title):
	plt.hist(d, bins='auto')
	plt.title(title)
	plt.show()

# INPUT: d : list of data points
# 		 title: the title to be displayed on the plot (string)
# OUTPUT: NULL
# PURPOSE: uses the plt library to plot a cdf (step plot) of the data set d
def plt_cdf(d, title):
	plt.step(np.sort(d), np.arange(np.sort(d).size))
	plt.title(title)
	plt.show() 

def main():
	f = open("full_results.pickle", "r")
	d = pickle.load(f)
	approx = d['APPROX']
	gsp = d['SET_COVER']
	mst = d['MST_SIMPLE']

	diff_gsp_approx = []
	diff_mst_approx = []
	# Find the differences between the list values
	for i in range(0,len(approx)):

		# print "APPROX on GRAPH"+str(i) + " : "+str(approx['GRAPH'+str(i)])
		# print "GSP on GRAPH"+str(i) + " : "+str(gsp['GRAPH'+str(i)])
		# print "MST on GRAPH"+str(i) + " : "+str(mst['GRAPH'+str(i)])

		#GSP should be larger in most cases
		diff_gsp_approx.append(gsp['GRAPH'+str(i)]-approx['GRAPH'+str(i)])
		#MST should be larger in most cases
		diff_mst_approx.append(mst['GRAPH'+str(i)]-approx['GRAPH'+str(i)])

	print "APPROX AND GSP: "+str(diff_gsp_approx)
	print "APPROX AND MST: "+str(diff_mst_approx)

	# Run a paired t test between list of approx values and gsp values
	paired_t_test(approx.values(), gsp.values())

	# PLT a histogram of their differences
	plt_hist(diff_gsp_approx, "Histogram: GSP - APPROX")
	plt_hist(diff_mst_approx, "Histogram: MST - APPROX")
	plt_cdf(diff_gsp_approx, "CDF (Step Plot): GSP - APPROX")
	plt_cdf(diff_mst_approx, "CDF (Step Plot): MST - APPROX")

if __name__=='__main__':main()