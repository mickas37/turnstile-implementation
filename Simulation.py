"""
This file contains the 'driver' for the simulation,
the main work is done in ParseGraphML.py and in the
files that contain the actual algorithms.

-S.A.M.
"""

import pickle
import time
import networkx as nx
import numpy
import random
import sys
import os
import copy
import fnss
import psutil
from OPTSolution import opt
from MSTHeuristics import MSTPerFlow, weightedMSTPerFlow, MST_simple
from ApproxAlg import approx
from SetCoverApprox import set_cover_approx

"""
Main Driver
"""
def main():
    ##############
    # PARAMETERS #
    ##############

    #Number of desired simulations
    number_of_simulations = 50

    #Which topology to run the simulations on, choose from {Geant, Tinet, Uunet, Dfn, Viatel}
    graph_names = ["Viatel"]

    #choose how the flows were generated, choose from {K-Shortest-Path-Flows, LP-Flows}
    type_of_graph = "LP-Flows"

    #choose the number of flows, options include {4, 100}
    number_of_comm = 4
    
    #applicable for k-shortest path, choose from {1,3,5}
    num_paths = 3

    ##################
    # END PARAMETERS #
    ##################

    flows = []

    #Variables for averaging performance
    u_ts_list = []
    w_ts_list = []    
    simple_ts_list = []
    opt_ts_list = []
    approx_ts_list = []
    set_cover_ts_list = []

    file_path_mst_old = "./MSTWeighted.txt"
    file_path_mst_update = "./MSTUpdated.txt"
    for i in range(0, number_of_simulations):
        print "\n BEGINNING ROUND "+str(i)+"\n"
        for t in range(0, len(graph_names)):
            print
            print "Running Simulation for "+graph_names[t]
            print

            f_path = ""
            ##########
            # FOR LP #
            ##########
            if type_of_graph == "LP-Flows":
                f_path = "Graphmls-for-simulations/"+type_of_graph+"/"+str(number_of_comm)+"-comm/"+graph_names[t]+"/Graph"+str(i)+"/"
            ##################
            # FOR K-SHORTEST #
            ##################            
            else:
                f_path = "Graphmls-for-simulations/"+type_of_graph+"/"+str(number_of_comm)+"-comm/"+str(num_paths)+"-path/"+graph_names[t]+"/Graph"+str(i)+"/"

            G = nx.read_gpickle(f_path+"G.gpickle")
            flowSet = {}
            with open(f_path+"flows.pickle") as f:
                flowSet = pickle.load(f)

            #flows is a list of flow numbers, i.e. if there are 3 flows, flows = [0,1,2]
            flows = []
            #commodities is a list of s,t flows with volume, i.e. commodities = [[s,t,volume],[s1,t1,volume1],...]
            commodities = {}
            for key,val in flowSet.items():
                flows.append(key)
                commodities[key] = val

            # u_mst_t0 = time.clock()
            # u_mst_sol, u_mst_total_benefit, u_mst_actual_benefit, u_mst_benefit_list = MSTPerFlow(G.copy(), copy.copy(flows))
            # print "Unweighted MST benefit: "+str(u_mst_actual_benefit)+" vs total benefit: "+str(u_mst_total_benefit) +" list: "+str(u_mst_benefit_list)+"\n"
            # u_ts_list.append(u_mst_sol)
            # u_mst_t1 = time.clock()

            # w_mst_t0 = time.clock()
            # w_mst_sol, w_mst_total_benefit, w_mst_actual_benefit, w_mst_benefit_list = weightedMSTPerFlow(G.copy(), copy.copy(flows))
            # print "Weighted MST benefit: "+str(w_mst_actual_benefit)+" vs total benefit: "+str(w_mst_total_benefit) +" list: "+str(w_mst_benefit_list)+"\n"
            # w_ts_list.append(w_mst_sol)
            # w_mst_t1 = time.clock()

            simple_mst_t0 = time.clock()
            simple_mst_sol, simple_mst_total_benefit, simple_mst_actual_benefit, simple_mst_benefit_list = MST_simple(G.copy(), copy.copy(flows))
            print "Simple MST benefit: "+str(simple_mst_actual_benefit)+" vs total benefit: "+str(simple_mst_total_benefit) +" list: "+str(simple_mst_benefit_list)+"\n"
            simple_ts_list.append(simple_mst_sol)
            simple_mst_t1 = time.clock()

            opt_t0 = time.clock()
            opt_ts_list.append(opt(G.copy(), copy.copy(flows)))
            print "Opt finished"
            #opt_ts_list.append(-1)
            opt_t1 = time.clock()

            approx_t0 = time.clock()
            approx_sol, approx_total_benefit, approx_actual_benefit, approx_benefit_list = approx(G.copy(), copy.copy(flows))
            print "Approx benefit: "+str(approx_actual_benefit)+" vs total benefit: "+str(approx_total_benefit)+ " list: "+str(approx_benefit_list)+"\n"
            if approx_actual_benefit != approx_total_benefit:
                sys.exit(1)
            approx_ts_list.append(approx_sol)
            approx_t1 = time.clock()

            set_cover_approx_t0 = time.clock()
            set_cover_sol, set_cover_total_benefit, set_cover_benefit, set_cover_benefit_list = set_cover_approx(G.copy(), copy.copy(flows), copy.copy(commodities))
            print "Set Cover benefit: "+str(set_cover_benefit)+" vs total benefit: "+str(set_cover_total_benefit)+ " list: "+str(set_cover_benefit_list)+"\n"
            if set_cover_benefit != set_cover_total_benefit:
                sys.exit(1)
            set_cover_ts_list.append(set_cover_sol)
            set_cover_approx_t1 = time.clock()

            print "TURNSTILES --- opt: "+str(opt_ts_list[-1])+ " | MST simple: " +str(simple_ts_list[-1])+" | Approx: "+str(approx_ts_list[-1])+" | Set Cover: "+str(set_cover_ts_list[-1])
            print "TIME --- opt: "+str(opt_t1-opt_t0)+"(s) | MST Weighted: "+str(simple_mst_t1-simple_mst_t0)+"(s) | Approx: "+str(approx_t1-approx_t0)+"(s)" + " | Set Cover: "+str(set_cover_approx_t1 - set_cover_approx_t0)+"(s)"

            if set_cover_ts_list[-1] < opt_ts_list[-1] or simple_ts_list[-1] < opt_ts_list[-1] or approx_ts_list[-1] < opt_ts_list[-1]:
                print"ERROR, OPT has been bested"
                sys.exit(1)

    
    # print "Avg Number of TS Unweighted: "+str(numpy.mean(u_ts_list))+ " | std dev: " + str(numpy.std(u_ts_list))
    # print "Avg Number of TS Weighted: "+str(numpy.mean(w_ts_list))+ " | std dev: " + str(numpy.std(w_ts_list))
    print "Avg Number of TS Simple: "+str(numpy.mean(simple_ts_list))+ " | std dev: " + str(numpy.std(simple_ts_list))
    print "Avg Number of TS Approx: "+str(numpy.mean(approx_ts_list))+ " | std dev: " + str(numpy.std(approx_ts_list))
    print "Avg Number of TS Set Cover: "+str(numpy.mean(set_cover_ts_list))+ " | std dev: " + str(numpy.std(set_cover_ts_list))
    print "Avg Number of TS OPT: "+str(numpy.mean(opt_ts_list))+" | std dev: "+str(numpy.std(opt_ts_list))+ " TIME: "+str(opt_t1-opt_t1)+"(s)"

    #comment out if you want to write to files
    #sys.exit(1)

    full_results = {}

    full_results['MST_SIMPLE'] = simple_ts_list
    full_results['APPROX'] = approx_ts_list
    full_results['SET_COVER'] = set_cover_ts_list
    full_results['OPT'] = opt_ts_list

    ##########################
    # WRITE TO RESULTS FILES #
    ##########################

    ############
    # LP FLOWS #
    ############
    if type_of_graph == "LP-Flows":
        file_path = "Simulation-Output/"+type_of_graph+"/"+str(number_of_comm)+"-comm/"+graph_names[0]+"/"
    ###################
    # K-SHORTEST PATH #
    ###################
    else:
        file_path = "Simulation-Output/"+type_of_graph+"/"+str(num_paths)+"-path/"+str(number_of_comm)+"-comm/"+graph_names[0]+"/"

    if not os.path.exists(file_path):
        os.makedirs(file_path)

    with open(file_path+"full_results.pickle", "w+") as full_f:
        pickle.dump(full_results, full_f)
    with open(file_path+"results-new.txt", "w+") as f:
        f.write( "Avg Number of TS MST Simple: "+str(numpy.mean(simple_ts_list))+" | std dev: "+str(numpy.std(simple_ts_list)))
        f.write( "\nAvg Number of TS Approx: "+str(numpy.mean(approx_ts_list))+ " | std dev: " + str(numpy.std(approx_ts_list)))
        f.write( "\nAvg Number of TS Set Cover: "+str(numpy.mean(set_cover_ts_list))+ " | std dev: " + str(numpy.std(set_cover_ts_list)))
        f.write( "\nAvg Number of TS OPT: "+str(numpy.mean(opt_ts_list))+" | std dev: "+str(numpy.std(opt_ts_list))+ " TIME: "+str(opt_t1-opt_t1)+"(s)" )

if __name__ == '__main__':main()
