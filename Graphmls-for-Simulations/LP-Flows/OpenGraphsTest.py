import pickle
import networkx as nx

G = nx.read_gpickle("4-comm/Tinet/Graph0/G.gpickle")
flows = {}
with open("4-comm/Tinet/Graph0/flows.pickle") as f:
	flows = pickle.load(f)

for edge in G.edges(data=True):
	print edge

for key,val in flows.items():
	print "Flow: "+str(key)+" with values: "+str(val)