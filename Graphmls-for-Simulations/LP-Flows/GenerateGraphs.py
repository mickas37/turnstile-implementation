import pickle
import time
import networkx as nx
import numpy
import random
import sys
import copy
import fnss
import psutil
import os
from ParseGraphML import parse_graphml
"""
Main Driver
"""
def main():
    ##############
    # PARAMETERS #
    ##############

    #Number of desired simulations
    number_of_simulations = 50

    #unused = ["BtNorthAmerica.graphml","HiberniaGlobal.graphml","Ntt.graphml","Telcove.graphml"]
    topology_zoo_topo_names = ["Viatel.graphml"]#"Geant.graphml","Uunet.graphml","Dfn.graphml","Viatel.graphml", "Tinet.graphml"]
    topo_commodity_file_names = ["Viatel.txt"]#"Geant.txt", "Uunet.txt", "Dfn.txt", "Viatel.txt", "Tinet.txt"]
    fnss_topologies = []
    nx_topologies = []

    for t in topology_zoo_topo_names:
         fnss_topologies.append(fnss.parse_topology_zoo("../../graphml/"+t))
         nx_topologies.append(nx.read_graphml("../../graphml/"+t))
    for i in range(0, number_of_simulations):
        print "\n BEGINNING ROUND "+str(i)+"\n"
        for t in range(0, len(topology_zoo_topo_names)):
            print
            print "Running Simulation for "+topology_zoo_topo_names[t]
            print
            #flows is a dict{flow_number: [s,t,v]}
            flows, G = parse_graphml(topo_commodity_file_names[t], nx_topologies[t])
            # for key, val in flows.items():
            #     print flows[key]
            # for edge in G.edges(data=True):
            #     print edge
            if not os.path.exists("100-comm/Viatel/Graph"+str(i)):
                os.makedirs("100-comm/Viatel/Graph"+str(i))
            nx.write_gpickle(G, "100-comm/Viatel/Graph"+str(i)+"/G.gpickle")
            pickle.dump(flows, open("100-comm/Viatel/Graph"+str(i)+"/flows.pickle", "wb" ) )


if __name__ == '__main__':main()


