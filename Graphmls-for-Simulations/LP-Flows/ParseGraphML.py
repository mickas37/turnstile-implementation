"""
Set fname in Simulation.py to the path of the graphml file you want parsed

This file powers the parsing of graphs and the actual simulation


-S.A.M.
"""

from SetFlows import set_flows
import networkx as nx
import matplotlib.pyplot as plt
import random
import pickle
import psutil
import fnss
import time
import os
import ast
import sys
EDGE_CAP = 1

"""
Converts a directed graph to undirected and adds all the
necessary values on each edge
"""
def convert_to_undirected(DiG):
    newG = nx.Graph()
    G = DiG.copy()

    for e in G.edges(data=True):
        if newG.has_edge(e[0], e[1]):
            if e[2].get('flows') is None:
                newG[e[0]][e[1]]['flows'] = {}
            else:
                newG[e[0]][e[1]]['flows'].update(e[2].get('flows'))
        else:
            newG.add_edge(e[0], e[1], flows=e[2].get('flows'))
        newG[e[0]][e[1]]['turnstile'] = False
        newG[e[0]][e[1]]['weight'] = 1
        newG[e[0]][e[1]]['capacity'] = e[2]['capacity']
    for e in newG.edges(data=True):
        newG[e[0]][e[1]]['flows'] = {int(k):i for k,i in e[2]['flows'].items()}

    return newG

def get_flow_volumes_fnss(number_to_sample, filename):
    #WE ARE JUST GOING TO PULL FROM THE APPROPRIATE FILE
    with open ("../../GeneratedFlowVolumes/"+filename, "r") as f:
        commodities = f.readlines()
        samp = random.sample(commodities, number_to_sample)
        comm_to_return = []
        for s in samp:
            s = ast.literal_eval(s)
            comm_to_return.append((s[0], s[1], s[2]))
        return comm_to_return


    # traffic_matrix = []
    # topo = fnss_G.copy()
    # #set some fixed capacity on all the links                                                                                        
    # fnss.set_capacities_constant(topo, 10, 'Gbps')
        	
    # nodes = topo.nodes()
        	
    # #SAMPLE NODES FROM VERTICES
    # origins = random.sample(nodes, number_to_sample)
    # list_of_neighbors = []
    # #make a list of neighbors to exclude from the next sample of destinations so that none of the origins can go straight to destinations
    # for o in origins:
    #     list_of_neighbors.extend(topo.neighbors(o))
    # destinations = random.sample(list(set(nodes)-set(list_of_neighbors)), number_to_sample)

    # #USE FNSS TO GENERATE THE VOLUMES
    # traffic_matrix = fnss.static_traffic_matrix(topo, 1.5, 1, origin_nodes=origins, destination_nodes=destinations)
        
    # #CREATE THE OD PAIRS
    # commodities = []
    # flows = []
    # for od1, od2 in traffic_matrix.od_pairs():
    #     commodities.append((od1,od2,traffic_matrix[(od1,od2)]))
        	
    # return commodities



"""
Generates the commodities using random sampling from the vertices.
Gets flow values using fnss static traffic matrix generator.
Uses CPLEX to solve the max flow problem in the graph with those flow values
and the given graph. This results in multi-commoditiy flows for our
algorithms to be tested on.
"""
def generate_commodities_and_flows(G_orig, topo_name):
    # Number of rounds * number to sample will be the total number of flows on the graph in the end
    # e.g. for 100 flows make number_of_rounds = 25 and number_to_sample=4
    # this has to be broken down or the CPLEX solver would never find an optimal solution
    number_of_rounds = 25
    number_to_sample = 4
    flows = {}
    G_init = G_orig.copy()
    for e in G_init.edges():
        G_init[e[0]][e[1]]['flows']={}
    for round_number in range(0, number_of_rounds):
        solution = False
    	while not solution:
    	    G = G_orig.copy()
    	    commodities = get_flow_volumes_fnss(number_to_sample, topo_name)
        	
            #print "Commodities: (source, destination, volume)"
            #for c in commodities:
            #    print str(c)
        	
            G = G.to_directed().copy()
        	        	
            instance = (G, commodities) 
        	
            
            if set_flows(instance):
                #SOLUTION WAS FOUND
                solution = True

                #ADD ON TO THE FLOWS LIST

                #####
                ##### TODO: CREATE traffic_matrix here and append the commodities
                """
                flow_matrix = {}
                for c in commodities:
                    flows.append(flow_counter)
                    traffic_matrix[c] = ((c[0], c[1]))
                    flow_counter+=1
                """
                flow_counter = 0        	
                for c in commodities:
                    flows[flow_counter + (round_number*4)] = c
                    #flows.append(flow_counter + (round_number*4))
                    flow_counter+=1

                #ADD THE BACK EDGES IN
                c_count = 0
                for c in commodities:
                    G.add_edge(c[1],c[0])
                    G[c[1]][c[0]]['capacity'] = EDGE_CAP
                    if G[c[1]][c[0]].has_key('flows'):
                        G[c[1]][c[0]]['flows'][str(c_count)] = 0.0
                    else:
                        G[c[1]][c[0]]['flows'] = {str(c_count):0.0}
                    c_count+=1

                #MERGE G BACK INTO G_INIT
                for e in G.edges(data=True):
                    for f_num, f_vol in e[2]['flows'].iteritems():
                        if G_init.has_edge(e[0],e[1]):
                            G_init[e[0]][e[1]]['flows'][str(int(f_num) + (4*round_number))] = f_vol
                        else: #it is a back edge and we need to create it
                            G_init.add_edge(e[0],e[1],capacity = EDGE_CAP, flows = {str(int(f_num) + (4*round_number)): f_vol})


                #PRINT OUT HOW THE FLOWS WERE GENERATED
                #FLOWS = {}
                #for f in flows:
                #    FLOWS[f] = nx.Graph()
                #    for e in G_init.edges(data=True):
                #        if str(f) in e[2]['flows']:
                #            FLOWS[f].add_edge(e[0], e[1], e[2])
                #
                #flow_counter = 0
                #for c in commodities:
                #    print "COMMODITY "+str(c) + " with flow number: "+str(flow_counter)
                #    for path in nx.all_simple_paths(FLOWS[flow_counter], source=c[0], target=c[1]):
                #        print path
                #    flow_counter+=1

    return flows, G_init

"""
Parses a topology zoo graph (.graphml file)
"""
def parse_graphml(topo_name, nx_topo):
    G = nx_topo.copy()

    #add capacity and convert the edge values to ints
    for e in G.edges(data=True):
        G.remove_edge(e[0],e[1])
        G.add_edge(int(e[0]),int(e[1]))
        G[int(e[0])][int(e[1])]['capacity'] = EDGE_CAP
        G.add_edge(int(e[1]),int(e[0]))
        G[int(e[1])][int(e[0])]['capacity'] = EDGE_CAP

    flows, G = generate_commodities_and_flows(G, topo_name)

    unG = convert_to_undirected(G)

    for e in unG.edges(data=True):
        if len(unG[e[0]][e[1]]['flows']) == 0:
            unG.remove_edge(e[0],e[1])

    return flows, unG



