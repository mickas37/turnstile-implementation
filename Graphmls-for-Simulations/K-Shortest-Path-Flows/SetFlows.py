import networkx as nx
import cplex
from cplex.exceptions import CplexError
import sys
import copy
#Set flows for a turnstile instance.
#   input: Networkx directed graph with 'capacity' key on each edge.
#   output: No return value.  Input graph is modified to include a
#           'flows' key for each edge which is a dict that maps each
#           commodity to its (non-zero) flow value on that edge.  If
#           that commodity does not have a flow on that edge, it is
#           not included in the dict
def set_flows(instance):
    #process input instance
    graph = instance[0]
    commodities = instance[1]

    try:
        #cplex solver instance
        lp = cplex.Cplex()
        
        #objective function - no objective function, just feasibility search
        lp.objective.set_sense(lp.objective.sense.minimize)
    
        #initialize cplex variables, e.g. 1t3c2 (node1 to node3 for commodity2)
        for edge in graph.edges():
            for commodity_num in range(len(commodities)):
                lp.variables.add(obj=[0], names = [_edge_variable(edge, commodity_num)])
                #add variable to objective function
                lp.objective.set_linear(_edge_variable(edge, commodity_num), 1.0)

        #capacity constraints
        for edge in graph.edges(data=True):
            linear_expr = [[], []]
            for commodity_num in range(len(commodities)):
                linear_expr[0].append(_edge_variable(edge, commodity_num))
                linear_expr[1].append(1.0)
            lp.linear_constraints.add(lin_expr=[linear_expr], senses='L', rhs=[edge[2]['capacity']])

        #conservation of flow constraints
        for commodity_num in range(len(commodities)):
            for node in graph.nodes():
                if node != commodities[commodity_num][0] and node != commodities[commodity_num][1]:
                    linear_expr = [[], []]
                    for edge in graph.edges():
                        #outgoing edge
                        if node == edge[0]:
                            linear_expr[0].append(_edge_variable(edge, commodity_num))
                            linear_expr[1].append(-1.0)
                        #incoming edge
                        if node == edge[1]:
                            linear_expr[0].append(_edge_variable(edge, commodity_num))
                            linear_expr[1].append(1.0)
                    lp.linear_constraints.add(lin_expr=[linear_expr], senses='E', rhs=[0])

        #demand constraints
        for commodity_num in range(len(commodities)):
            linear_expr = [[], []]
            for edge in graph.edges():
                #edge from commodity's source
                if edge[0] == commodities[commodity_num][0]:
                    linear_expr[0].append(_edge_variable(edge, commodity_num))
                    linear_expr[1].append(1.0)
                #edge into commodity's source
                if edge[1] == commodities[commodity_num][0]:
                    linear_expr[0].append(_edge_variable(edge, commodity_num))
                    linear_expr[1].append(-1.0)
            lp.linear_constraints.add(lin_expr=[linear_expr], senses='E', rhs=[commodities[commodity_num][2]])

        #solve lp
        lp.solve()
        #lp.write('model.lp')

        if lp.solution.get_status() == 1:
            #load valid solution into graph - add 'flows' key to each edge which is dict: commodity_num -> flow_val (if > 0)
            for edge in graph.edges(data=True):
                flows = {}
                for commodity_num in range(len(commodities)):
                    flow_val = lp.solution.get_values(_edge_variable(edge, commodity_num))
                    if flow_val > 0:
                        flows[str(commodity_num)] = flow_val
                edge[2]['flows'] = flows
            return 1
        else:
            print('No optimal solution found.')
            return 0
        
    except CplexError as exc:
        #print(exc)
        return 0

def _edge_variable(edge, commodity):
    return str(edge[0]) + 't' + str(edge[1]) + 'c' + str(commodity)

