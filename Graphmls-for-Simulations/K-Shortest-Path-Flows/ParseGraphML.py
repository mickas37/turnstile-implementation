"""
Set fname in Simulation.py to the path of the graphml file you want parsed

This file powers the parsing of graphs and the actual simulation


-S.A.M.
"""

from SetFlows import set_flows
import networkx as nx
import matplotlib.pyplot as plt
import random
import pickle
import psutil
import fnss
import time
import os
import ast
import sys
from itertools import islice
EDGE_CAP = 1

"""
Converts a directed graph to undirected and adds all the
necessary values on each edge
"""
def convert_to_undirected(DiG):
    newG = nx.Graph()
    G = DiG.copy()

    for e in G.edges(data=True):
        if newG.has_edge(e[0], e[1]):
            if e[2].get('flows') is None:
                newG[e[0]][e[1]]['flows'] = {}
            else:
                newG[e[0]][e[1]]['flows'].update(e[2].get('flows'))
        else:
            newG.add_edge(e[0], e[1], flows=e[2].get('flows'))
        newG[e[0]][e[1]]['turnstile'] = False
        newG[e[0]][e[1]]['weight'] = 1
        newG[e[0]][e[1]]['capacity'] = e[2]['capacity']
    for e in newG.edges(data=True):
        newG[e[0]][e[1]]['flows'] = {int(k):i for k,i in e[2]['flows'].items()}

    return newG

def get_flow_volumes_fnss(number_to_sample, filename):
    #WE ARE JUST GOING TO PULL FROM THE APPROPRIATE FILE
    with open ("../../GeneratedFlowVolumes/"+filename, "r") as f:
        commodities = f.readlines()
        samp = random.sample(commodities, number_to_sample)
        comm_to_return = []
        for s in samp:
            s = ast.literal_eval(s)
            comm_to_return.append((s[0], s[1], s[2]))
        return comm_to_return




def k_shortest_paths(G, source, target, k, weight=None):
    return list(islice(nx.shortest_simple_paths(G, source, target, weight=weight), k))



"""
Generates the commodities using random sampling from the vertices.
Gets flow values using fnss static traffic matrix generator.
Uses CPLEX to solve the max flow problem in the graph with those flow values
and the given graph. This results in multi-commoditiy flows for our
algorithms to be tested on.
"""
def generate_commodities_and_flows(G_orig, topo_name, k):
    number_of_flows = 100
    flows = {}
    G_init = G_orig.copy()
    for e in G_init.edges():
        G_init[e[0]][e[1]]['flows']={}

    G = G_orig.copy()
    commodities = get_flow_volumes_fnss(number_of_flows, topo_name)
	
    # print "Commodities: (source, destination, volume)"
    # for c in commodities:
    #    print str(c)

    #MAKE SURE EVERY EDGE HAS A FLOWS ATTRIBUTE
    for e in G.edges(data = True):
        G[e[0]][e[1]]['flows']={}    	


    flow_count = 0
    #CREATE THE FLOWS AND PUT THEM IN THE GRAPH
    for c in commodities:
        for path in k_shortest_paths(G, c[0], c[1], k):
            for i in range(0, len(path)-1):
                if G[path[i]][path[i+1]].has_key('flows'):
                    G[path[i]][path[i+1]]['flows'][str(flow_count)] = 1.0
                else:
                    G[path[i]][path[i+1]]['flows'] = {str(flow_count):1.0}
        flow_count+=1

    flow_counter = 0        	
    for c in commodities:
        flows[flow_counter] = c
        #flows.append(flow_counter + (round_number*4))
        flow_counter+=1


    #ADD THE BACK EDGES IN
    c_count = 0
    for c in commodities:
        G.add_edge(c[1],c[0])
        G[c[1]][c[0]]['capacity'] = EDGE_CAP
        if G[c[1]][c[0]].has_key('flows'):
            G[c[1]][c[0]]['flows'][str(c_count)] = 0.0
        else:
            G[c[1]][c[0]]['flows'] = {str(c_count):0.0}
        c_count+=1

    #MERGE G BACK INTO G_INIT
    for e in G.edges(data=True):
        # print str(e)
        for f_num, f_vol in e[2]['flows'].iteritems():
            if G_init.has_edge(e[0],e[1]):
                G_init[e[0]][e[1]]['flows'][str(f_num)] = f_vol
            else: #it is a back edge and we need to create it
                G_init.add_edge(e[0],e[1],capacity = EDGE_CAP, flows = {str(f_num): f_vol})


    return flows, G_init

"""
Parses a topology zoo graph (.graphml file)
"""
def parse_graphml(topo_name, nx_topo, k):
    G = nx_topo.copy()

    #add capacity and convert the edge values to ints
    for e in G.edges(data=True):
        G.remove_edge(e[0],e[1])
        G.add_edge(int(e[0]),int(e[1]))
        G[int(e[0])][int(e[1])]['capacity'] = EDGE_CAP
        G.add_edge(int(e[1]),int(e[0]))
        G[int(e[1])][int(e[0])]['capacity'] = EDGE_CAP

    flows, G = generate_commodities_and_flows(G, topo_name, k)

    unG = convert_to_undirected(G)

    for e in unG.edges(data=True):
        if len(unG[e[0]][e[1]]['flows']) == 0:
            unG.remove_edge(e[0],e[1])

    return flows, unG



