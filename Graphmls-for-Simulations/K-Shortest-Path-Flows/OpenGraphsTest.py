import pickle
import networkx as nx

G = nx.read_gpickle("100-comm/3-path/Tinet/Graph0/G.gpickle")
flows = {}
with open("100-comm/3-path/Tinet/Graph0/flows.pickle") as f:
	flows = pickle.load(f)

for edge in G.edges(data=True):
	print edge

for key,val in flows.items():
	print "Flow: "+str(key)+" with values: "+str(val)

print "Printing flow 1"
for e in G.edges(data=True):
	if 1 in e[2]['flows']:
		print e