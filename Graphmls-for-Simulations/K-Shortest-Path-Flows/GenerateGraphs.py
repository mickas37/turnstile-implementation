import pickle
import time
import networkx as nx
import numpy
import random
import sys
import copy
import fnss
import psutil
import os
from ParseGraphML import parse_graphml
"""
Main Driver
"""
def main():
    ##############
    # PARAMETERS #
    ##############

    #Number of desired simulations
    number_of_simulations = 50

    #number of flows each topology should have
    number_of_flows = 100

    #the topology that we want to run this on from {Geant, Uunet, Dfn, Viatel, Tinet}
    graph_name = "Tinet"

    #number of paths we want each flow to have
    k = 5

    ##################
    # END PARAMETERS #
    ##################s
    #unused = ["BtNorthAmerica.graphml","HiberniaGlobal.graphml","Ntt.graphml","Telcove.graphml"]
    topology_zoo_topo_names = [graph_name+".graphml"]#"Geant.graphml","Uunet.graphml","Dfn.graphml","Viatel.graphml", "Tinet.graphml"]
    topo_commodity_file_names = [graph_name+".txt"]#"Geant.txt", "Uunet.txt", "Dfn.txt", "Viatel.txt", "Tinet.txt"]
    fnss_topologies = []
    nx_topologies = []

    for t in topology_zoo_topo_names:
         fnss_topologies.append(fnss.parse_topology_zoo("../../graphml/"+t))
         nx_topologies.append(nx.read_graphml("../../graphml/"+t))
    for i in range(0, number_of_simulations):
        print "\n BEGINNING ROUND "+str(i)+"\n"
        for t in range(0, len(topology_zoo_topo_names)):
            print
            print "Running Simulation for "+topology_zoo_topo_names[t]
            print
            #flows is a dict{flow_number: [s,t,v]}
            flows, G = parse_graphml(topo_commodity_file_names[t], nx_topologies[t], k)
            if not os.path.exists(str(number_of_flows)+"-comm/"+str(k)+"-path/"+graph_name+"/Graph"+str(i)):
                os.makedirs(str(number_of_flows)+"-comm/"+str(k)+"-path/"+graph_name+"/Graph"+str(i))
            nx.write_gpickle(G, str(number_of_flows)+"-comm/"+str(k)+"-path/"+graph_name+"/Graph"+str(i)+"/G.gpickle")
            pickle.dump(flows, open(str(number_of_flows)+"-comm/"+str(k)+"-path/"+graph_name+"/Graph"+str(i)+"/flows.pickle", "wb" ) )


if __name__ == '__main__':main()


