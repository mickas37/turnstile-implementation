import networkx as nx
import fnss
import random

def get_flow_volumes_fnss(fnss_G, out_file, number_to_sample):
    traffic_matrix = []
    topo = fnss_G.copy()

    #set some fixed capacity on all the links                                                                                                                       
    fnss.set_capacities_constant(topo, 2, 'Gbps')

    nodes = topo.nodes()

    #SAMPLE NODES FROM VERTICES                                                                                                                                     
    origins = random.sample(nodes, number_to_sample)
    list_of_neighbors = []
    #make a list of neighbors to exclude from the next sample of destinations so that none of the origins can go straight to destinations                           
    for o in origins:
        list_of_neighbors.extend(topo.neighbors(o))
    destinations = random.sample(list(set(nodes)-set(list_of_neighbors)), number_to_sample)

    #USE FNSS TO GENERATE THE VOLUMES                                                                                                                               
    traffic_matrix = fnss.static_traffic_matrix(topo, 1.5, 1, origin_nodes=origins, destination_nodes=destinations)

    #CREATE THE OD PAIRS                                                                                                                                            
    commodities = []
    flows = []
    for od1, od2 in traffic_matrix.od_pairs():
        commodities.append((od1,od2,traffic_matrix[(od1,od2)]))
    with open(out_file, "a") as f:
        for c in commodities:
            f.write(str(c)+"\n")

def main():
    f_name = "Geant.graphml"
    output_f_name = "Geant.txt"
    number_to_sample = 2 #note this is number_of_comm^2
    G = fnss.parse_topology_zoo("../graphml/"+f_name)
    get_flow_volumes_fnss(G, output_f_name, number_to_sample)

if __name__=='__main__':main()
