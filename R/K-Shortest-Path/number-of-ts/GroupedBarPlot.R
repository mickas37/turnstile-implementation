# Grouped Bar Plot
pdf("K-Shortest-topo-zoo-100comm-barplot.pdf")

#these are pulled from the results-updated.txt and the results-simple.txt files
#approx, set cover, mst simple
One <- c(17.94, 17.98, 27.28)
Three <- c(34.3, 39.78, 46.76)
Five <- c(38.84, 47.56, 52.12)

data <- cbind(One,Three,Five)
colnames(data) <- c("k=1", "k=3", "k=5")

#dens = c(10, 20, 10)
#ang = c(45, 45, 135)
cols = c("gray50","gray75","gray100")

par(mar=c(8.1,4.1,4.1,4.1), xpd=TRUE, family="Times")

barplot(data, axes=TRUE, cex.names=1.35, cex.axis=1.35, cex.lab=1.5, ylab="Turnstiles Placed", ylim=c(0,60), xlim=c(0,12),col=cols, ,xlab="k-shortest path routing", beside=TRUE)

legend("bottomleft", bty="n", inset=c(-0.0,-0.3), cex=1.35, legend=c("TP-Approx","GSP","TP-MaxST"), fill=cols, horiz=TRUE, text.width=c(1,4,3))

axis(1, labels=FALSE, tck=0)

dev.off()