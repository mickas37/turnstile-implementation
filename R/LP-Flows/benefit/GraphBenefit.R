pdf("lp-flows-Benefit.pdf")
x <- c(1:37)
approx_ben <- c(33, 59, 71, 82, 91, 99, 106, 112, 116, 119, 122, 125, 127, 129, 131, 133, 134, 135, 136, 137, 138, 139, 140, 141, 141, 141, 141, 141, 141, 141, 141, 141, 141, 141, 141, 141, 141)
mst_weighted_ben <- c(33, 36, 41, 46, 52, 72, 74, 76, 90, 93, 95, 97, 98, 99, 100, 102, 111, 111, 119, 121, 123, 124, 125, 126, 130, 130, 132, 133, 133, 134, 135, 135, 137, 138, 139, 140, 141)
set_cover <- c(33, 59, 71, 82, 91, 99, 106, 112, 116, 119, 122, 125, 127, 129, 131, 133, 135, 136, 137, 138, 139, 140, 141, 141, 141, 141, 141, 141, 141, 141, 141, 141, 141, 141, 141, 141, 141)

par(mar=c(8.1,4.1,4.1,4.1), xpd=TRUE, family="Times")

plot(x, x, cex.axis=1.35,cex.lab=1.5, ylim=c(0,150), xlab = "Turnstile Placed", ylab = "Monitoring Benefit", type="n")

points(x, approx_ben, type="l", lty=1, lwd = 2, col="darkgreen")
points(x, set_cover, type="l", lty=5, lwd = 2, col="blue")
points(x, mst_weighted_ben, type="l", lty=3, lwd = 4, col="black")
legend("bottomleft", bty="n", inset=c(-0.0, -0.3), cex = 1.35, legend=c("TP-Approx", "GSP", "TP-MST"), lty=c(1,5,3), lwd=c(2,2,4), col=c("darkgreen","blue","black"), horiz=TRUE, text.width=c(10, 10, 8))

dev.off()