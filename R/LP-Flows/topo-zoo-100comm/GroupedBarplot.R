# Grouped Bar Plot
pdf("lp-flows-topo-zoo-100comm-barplot.pdf")

#these are pulled from the results-updated.txt and the results-simple.txt files
#approx, set cover, mst simple
Viatel <- c(12.9,13.02,17.36)
DFN <- c(25.0, 26.82, 33.4)
UUNET <- c(25.9, 26.96, 36.24)
GEANT <- c(21.18, 22.28, 28.64)
Tinet <- c(22.5, 23.94, 31.92)

data <- cbind(GEANT, UUNET, DFN, Viatel, Tinet)

#dens = c(10, 20, 10)
#ang = c(45, 45, 135)
cols = c("gray50","gray75","gray100")

par(mar=c(8.1,4.1,4.1,4.1), xpd=TRUE, family="Times")

barplot(data, axes=TRUE, cex.names=1.35, cex.axis=1.35, cex.lab=1.5, ylab="Turnstiles placed", ylim=c(0,45), xlim=c(0,20),col=cols, ,xlab="Network", beside=TRUE)

legend("bottomleft", bty="n", inset=c(-0.0,-0.3), cex=1.35, legend=c("TP-Approx","GSP","TP-MaxST"), fill=cols, horiz=TRUE, text.width=c(1.5,6,4.9))

axis(1, labels=FALSE, tck=0)

dev.off()