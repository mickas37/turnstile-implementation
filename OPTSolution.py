"""
This file contains the implementation of the 
OPT solution to the turnstile problem. It is 
NP-Hard, so the implementation relies on generating
all combinations of edges in the graph.

-S.A.M.
"""

import networkx as nx
from MSTHeuristics import createSubGraphs
import itertools

"""
tests if there are any uncovered cycles
"""
def testSolution(combination, flowsArr):
    for f_number, flow in flowsArr.iteritems():
        flowCopy = flow.copy()
        for e in combination:
            if flowCopy.has_edge(*e):
                flowCopy.remove_edge(*e)
        if len(nx.cycle_basis(flowCopy)) > 0:
            return False

    return True

"""
Generates all combinations of edges and tests
to see if they are correct starting with the smallest
and ending with the largest
"""
def opt(G, flows):
    flowsArr = createSubGraphs(G, flows)  
    back_edges = []
    # Find all of teh back edges (back edges have a volume of 0.0)
    for e in G.edges(data=True):
        if 0.0 in e[2]['flows'].values():
            back_edges.append((e[0],e[1]))

    set_of_edges = G.edges()

    #remove the back edges
    #these are not part of the actual graph so we do not want to include
    #them in the solution
    for e in back_edges:
        set_of_edges.remove(e)

    # 2) Create all combinations of edges without the back edges
    combinations = []
    for i in range(0, len(G.edges())+1):
        for c in itertools.combinations(set_of_edges, i):
            # 2) test each combination to see if it is a solution
            if testSolution(list(c), flowsArr):
                #print "OPT: "+str(c)
                return len(c)

