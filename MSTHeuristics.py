"""
This file contains the implementation of the two heuristics
we propose in the paper. They both find MSTs for each flow
and place turnstiles on the edges not in those MSTs 
(effectively covering each cycle). One of them randomly 
assigns edge weights for the MST and the other sets the 
weights to be based on the number of flows on that edge.

-S.A.M.
"""
from TSPlacementBenefit import calc_benefit, calc_total_number
import networkx as nx
import random
import copy

#constant representing infinity
INF = 9999999

"""
Tests to ensure that the solution is valid
"""
def testSolution(combination, flowsArr):
    for f_number, flow in flowsArr.iteritems():
        flowCopy = flow.copy()
        for e in combination:
            if flowCopy.has_edge(*e):
                flowCopy.remove_edge(*e)
        if len(nx.cycle_basis(flowCopy)) > 0:
            return False

    return True

"""
Takes a graph and MST, places turnstiles on edges in graph that aren't in MST                                
"""
def placeTurnstiles(G, MST):
    for e in G.edges_iter():
        if not MST.has_edge(e[0], e[1]):
            G.edge[e[0]][e[1]]['turnstile'] = True
    return G


"""
Creates a dictionary of {flow number: graph of the flow}
"""
def createSubGraphs(G, flows):
    flowMSTs = {}
    for f in flows:
        flowMSTs[f] = nx.Graph()
        for e in G.edges(data=True):
            if f in e[2]['flows']:
                flowMSTs[f].add_edge(e[0], e[1], e[2])
    return flowMSTs

"""
finds MST for every flow with random edge weights
"""
def MSTPerFlow(G, flows):
    flowMSTs = createSubGraphs(G, flows)
    total_benefit = calc_total_number(flowMSTs)
    mst_benefit = 0
    list_of_benefits = []
    prev_benefit = 0
    for f, flowG in flowMSTs.iteritems():
        for e in flowG.edges(data=True):
            if 0.0 in e[2]['flows'].values():
                e[2]['weight'] = 0
                G[e[0]][e[1]]['weight'] = 0
            else:
                e[2]['weight'] = 1
                G[e[0]][e[1]]['weight'] = 1
        MST = nx.minimum_spanning_tree(flowG)
        flowMSTs[f] = placeTurnstiles(flowG, MST)

    tsCount = 0
    for f, flowG in flowMSTs.iteritems():
        for e in flowG.edges(data = True):
            if e[2]['turnstile'] == True:
                if G.edge[e[0]][e[1]]['turnstile'] == False:
                    G.edge[e[0]][e[1]]['turnstile'] = True
                    ben_amount = calc_benefit(G, flows, e)
                    list_of_benefits.append(ben_amount+prev_benefit)
                    prev_benefit = ben_amount + prev_benefit
                    mst_benefit += ben_amount
                    tsCount = tsCount+1
    return tsCount, total_benefit, mst_benefit, list_of_benefits

"""
finds MST for every flow with edge weights adjusted for number of flows on that edge                                                  
"""
def weightedMSTPerFlow(G, flows):
    flowMSTs = createSubGraphs(G, flows)
    total_benefit = calc_total_number(flowMSTs)
    mst_benefit = 0
    list_of_benefits = []
    edge_chosen_order = []
    prev_benefit = 0
    for f, flowG in flowMSTs.iteritems():
        for e in flowG.edges(data=True):
            e[2]['weight'] = len(e[2]['flows'])
            G[e[0]][e[1]]['weight'] = len(e[2]['flows'])
    for f in flows:
        MST = nx.minimum_spanning_tree(flowMSTs[f], weight="weight")
        flowMSTs[f] = placeTurnstiles(flowMSTs[f], MST)
        for e in flowMSTs[f].edges(data=True):
            if e[2]['turnstile'] == True:
                for i in flows:
                    if flowMSTs[i].has_edge(e[0],e[1]):
                        flowMSTs[i].edge[e[0]][e[1]]['weight'] = flowMSTs[i].edge[e[0]][e[1]]['weight'] + 1
                        
                G.edge[e[0]][e[1]]['weight'] = G.edge[e[0]][e[1]]['weight'] + 1

    tsCount = 0
    for f, flowG in flowMSTs.iteritems():
        for e in flowG.edges(data = True):
            if e[2]['turnstile'] == True:
                if G.edge[e[0]][e[1]]['turnstile'] == False:
                    G.edge[e[0]][e[1]]['turnstile'] = True
                    ben_amount = calc_benefit(G, flows, e)
                    list_of_benefits.append(ben_amount+prev_benefit)
                    prev_benefit = prev_benefit+ben_amount
                    mst_benefit += ben_amount
                    tsCount = tsCount+1
    

    return tsCount, total_benefit, mst_benefit, list_of_benefits


"""
finds a random MST for each commodity then sets the chosen
turnstile location edge weights to infinity so that they will
be chosen by other commodities as well to be turnstile locations

NOTE: this is the algorithm described in the camera ready version
of IFIP                                                
"""
def MST_simple(G, flows):
    flowMSTs = createSubGraphs(G, flows)
    total_benefit = calc_total_number(flowMSTs)
    mst_benefit = 0
    list_of_benefits = []
    edge_chosen_order = []
    prev_benefit = 0
    for f, flowG in flowMSTs.iteritems():
        for e in flowG.edges(data=True):
            if 0.0 in e[2]['flows'].values():
                e[2]['weight'] = 0
                G[e[0]][e[1]]['weight'] = 0
            else:
                e[2]['weight'] = 1
                G[e[0]][e[1]]['weight'] = 1
    for f in flows:
        MST = nx.minimum_spanning_tree(flowMSTs[f], weight="weight")
        flowMSTs[f] = placeTurnstiles(flowMSTs[f], MST)
        for e in flowMSTs[f].edges(data=True):
            if e[2]['turnstile'] == True:
                for i in flows:
                    if flowMSTs[i].has_edge(e[0],e[1]):
                        flowMSTs[i].edge[e[0]][e[1]]['weight'] = INF
                G.edge[e[0]][e[1]]['weight'] = INF

    tsCount = 0
    sol_set = []
    for f, flowG in flowMSTs.iteritems():
        for e in flowG.edges(data = True):
            if e[2]['turnstile'] == True:
                if G.edge[e[0]][e[1]]['turnstile'] == False:
                    G.edge[e[0]][e[1]]['turnstile'] = True
                    ben_amount = calc_benefit(G, flows, e)
                    list_of_benefits.append(ben_amount+prev_benefit)
                    prev_benefit = prev_benefit+ben_amount
                    mst_benefit += ben_amount
                    tsCount = tsCount+1
                    sol_set.append((e[0],e[1]))
    print "MST-Simple solution is valid: "+str(testSolution(sol_set, flowMSTs))    
    if not testSolution(sol_set, flowMSTs):
        print "Updated MST Heuristic did not return a correct solution:"
        print str(sol_set)
        for e in G.edges(data=True):
            print e
    return tsCount, total_benefit, mst_benefit, list_of_benefits

def main():
    G = nx.Graph()

    ##########
    # TEST 2 #
    ##########
    # V = [0,1,2,3,4,5]
    # commodities = {0: [0,4,.5], 1:[1,5,.7],2:[1,3,.9]}
    # flows = [0,1,2]
    # E = [(0,1, {'flows':{0:1.0}, 'turnstile':False}), (1,2, {'flows':{0:1.0, 1:1.0, 2:1.0}, 'turnstile':False}), (1,3, {'flows':{0:1.0, 2:0.0}, 'turnstile':False}),
    #     (2,3, {'flows':{0:1.0, 2:10}, 'turnstile':False}), (3,4, {'flows':{0:1.0}, 'turnstile':False}), (2,5, {'flows':{1:1.0}, 'turnstile':False}), 
    #     (4,0, {'flows':{0:0.0}, 'turnstile':False}), (5,1, {'flows':{1:0.0}, 'turnstile':False})]
    # G.add_nodes_from(V)
    # G.add_edges_from(E)
    V = [0,1,2,3,4,5]
    commodities = {0: [0,4,.5], 1:[1,5,.7]}
    flows = [0,1]
    E = [(0,1,{'flows':{0:1.0},'turnstile':False}), (1,2,{'flows':{0:1.0},'turnstile':False}), (2,3,{'flows':{0:1.0, 1:1.0},'turnstile':False}),
    (3,0,{'flows':{0:0.0},'turnstile':False}),(3,4,{'flows':{1:1.0},'turnstile':False}),(4,5,{'flows':{1:1.0},'turnstile':False}),(5,2,{'flows':{1:0.0},'turnstile':False})]
    G.add_nodes_from(V)
    G.add_edges_from(E)
    
    for e in G.edges(data=True):
        print e
    set_cover_sol, total_benefit, set_cover_benefit, list_of_benefits = MST_simple(G, flows)
    for e in G.edges(data = True):
        print e
    print str(set_cover_sol)

if __name__=='__main__':main()






