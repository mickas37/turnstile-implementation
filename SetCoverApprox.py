"""
This file contains the code for the set cover approximation
to the turnstile problem. 

The solution decomposes pathlets from the flows in the graph.
Then, using the pathlets and the flows on each edge it tries
to find a set cover solution that includes every flow, with the 
minimum number of edges.

-S.A.M.
"""
from sets import Set
import networkx as nx
from TSPlacementBenefit import calc_benefit, calc_total_number
from ApproxAlg import createSubGraphs
import copy
import sys

"""
Method for testing correctness
"""
def testSolution(combination, flowsArr):
    #print str(flowsArr.keys())
    for f_number in flowsArr.keys():
        (flow, (s, t)) = flowsArr[f_number]
        flowCopy = flow.copy().to_undirected()
        for e in combination:
            if flowCopy.has_edge(*e):
                flowCopy.remove_edge(*e)
        #make sure there are no cycles
        if len(nx.cycle_basis(flowCopy)) > 0:
            return False
        #make sure that the remaining path doesn't go from s->t
        #we removed back edges for this case, so we can't allow any path from s->t
        if nx.has_path(flowCopy, s, t):
            return False

    return True

"""
Gets the flows from the graph and creates an array of new graphs containing just those flows
"""
def createSubGraphsSetCover(G, flows):
    flowMSTs = {}
    for f in flows:
        flowMSTs[f] = nx.DiGraph()
        for e in G.edges(data=True):
            if f in e[2]['flows']:
                flowMSTs[f].add_edge(e[0], e[1], e[2])
                #flowMSTs[f][e[0]][e[1]]['capacity'] = G[e[0]][e[1]]['flows'][f]
    return flowMSTs

"""
INPUT:
  Graph G (subgraph for a particular flow)
  Source vertex s, randomly chosen from visited set
  Visited set V, we are looking for any node from s, that is not s, to find
OUTPUT:
  Path of vertices from s to some other node in visited set V
"""
def bfs(G, s, V):
    q = []
    q.append([s])
    while len(q) > 0:
        #get the full path we are going to be starting from
        path = q.pop(0)
        #get the most recently visited node on that path (the one we are at)
        node = path[-1]
        #if that node is part of the visited set without the source vertex then we return that path
        if node in (V - Set([s])):
            return path
        #otherwise we create new paths for all of our neighbors and append those to our queue
        for n in G.neighbors(node):
            new_path = list(path)
            new_path.append(n)
            #If there are no duplicate items we want to append it
            # - this helps ensure that we don't create loops
            if len (Set(new_path)) == len(new_path):
                q.append(new_path)
    #I don't think we will ever get to this point with our input, but I want to cover my butt just in case
    return []

"""
INPUT:
  Undirected graph G
  List of subgraphs (each is one flow)
  traffic_matrix containing s,t pairs for each flow
OUTPUT:
  List of new single path flows decomposed from multi-path flows
"""
def decompose(commodities, flowSets):
    new_flows = []
    for flow, [s,t,v] in commodities.items():
        #print "Decomposing flow "+ str(flow) +  " with source "+str(s) +", destination "+str(t) +", and volume "+str(v)
        Visited = Set([s,t])
        # print str(Visited)
        sub_G = copy.copy(flowSets[flow])

        # make sure all edges bidirectional
        for e in sub_G.edges(data = True):
            # print str(e)
            sub_G.add_edge(e[1],e[0],e[2])

        #Do a BFS using the node class to build a tree data structure
        while(len(sub_G.edges())>0):

            #get a new path
            path = bfs(sub_G, s, Visited)

            #add the nodes in the new path to visited
            Visited = Visited.union(Set(path))

            #add our new path as a new flow
            new_flows.append(path)
            for i in range(0, len(path)-1):
                # we need to remove both edges once one is traversed
                sub_G.remove_edge(path[i], path[i+1])
                sub_G.remove_edge(path[i+1], path[i])

            #reset s to a node that is both in the graph and in our Visited set
            for e in sub_G.edges(data = True):
                if e[0] in Visited:
                    s = e[0]
                    break
                elif e[1] in Visited:
                    s = e[1]
                    break
    return new_flows

"""
INPUT:
  Graph G
OUTPUT:
  G without the back edges that were added when the graph was created
   - we remove a back edge by removing any flow with volume 0.0 from that edge
"""
def remove_back_edges(G):
    for e in G.edges(data=True):
        for f in G[e[0]][e[1]]['flows'].keys():
            if G[e[0]][e[1]]['flows'][f] == 0.0:
                G[e[0]][e[1]]['flows'].pop(f)
    return G
"""
INPUT:
  flows is the universe (all the flows)
  edges is the set of sets, we want the smallest set of edges that will cover all flows
OUTPUT:
  approximation of best set cover solution as a set of edges
"""
def set_cover(G, flows, edges, commodities, G_orig, flows_orig):
    sol = []
    benefit = 0
    list_of_benefits = []
    prev_benefit = 0

    while len(flows)>0:
        i = 0
        selected_edge_index = 0
        best_intersection = ()
        for e in edges:
            flows_on_edge = set(e[2]['flows'])
            if len(flows.intersection(flows_on_edge)) > len(best_intersection):
                best_intersection = flows.intersection(flows_on_edge)
                selected_edge_index = i
            i+=1

        selected_edge = edges[selected_edge_index]
        G[selected_edge[0]][selected_edge[1]]['turnstile']=True
        G_orig[selected_edge[0]][selected_edge[1]]['turnstile']=True
        curr_benefit = calc_benefit(G_orig,flows_orig,edges[selected_edge_index])
        list_of_benefits.append(curr_benefit+prev_benefit)
        benefit += curr_benefit
        prev_benefit = curr_benefit + prev_benefit
        sol.append(selected_edge)
        del edges[selected_edge_index]
        for f in best_intersection:
            flows.remove(f)

    return len(sol), sol, benefit, list_of_benefits    
       
def print_graph(G):
    for e in G.edges(data = True):
        print e

"""
Creates the new flows and then sends them to 
set_cover to get an approximate solution
INPUT:
  G, the graph
  directedG, directed version of the graph
  flows, list of flows ([0,1,2,...n])
  commodities, list of commodities = [[s,t,volume], ....]
"""            
def set_cover_approx(G, flows, commodities):
    #make copies to calculate the benefit of each TS placement
    G_orig = G.copy()
    flows_orig = copy.copy(flows)
    # print_graph(G_orig)

    #remove the back edges
    G = remove_back_edges(G)

    #get a list of all of the flows
    flowSet = createSubGraphsSetCover(G, flows)
    test_sol_flowSet = createSubGraphsSetCover(G, flows)
    flow_count = 0
    for c, val in commodities.iteritems():
        test_sol_flowSet[flow_count]= (test_sol_flowSet[flow_count].copy(), (val[0],val[1]))
        flow_count +=1

    #generate all the paths for each flow
    all_paths = decompose(commodities, copy.copy(flowSet))   

    #reset all flows to none since we are going to be making new ones
    for e in G.edges(data=True):
        G[e[0]][e[1]]['flows']=[]

    flow_num = 0
    flows = []
    #create the new flows
    extra_edge_count = 1
    for p in all_paths:
        #add the flow to those edges
        for i in range(0, len(p)-1):
            if G.has_edge(p[i],p[i+1]):
                G[p[i]][p[i+1]]['flows'].append(flow_num)
            else:
                G[p[i+1]][p[i]]['flows'].append(flow_num)
        #add that flow to our list
        flows.append(flow_num)
        flow_num+=1


    Flows = createSubGraphs(G_orig, flows_orig)
    #get the total benefit for the graph
    total_benefit = calc_total_number(Flows)

    #get the solution to the Set Cover approx with the new flows and graph
    set_cover_sol, full_sol_edge_list, set_cover_benefit, list_of_benefits = set_cover(G, set(flows), list(G.edges(data=True)), commodities, G_orig, flows_orig)
    

    #test solution
    test_sol = []
    for s in full_sol_edge_list:
        test_sol.append((s[0], s[1]))

    correct = testSolution(test_sol, test_sol_flowSet)
    print "Set cover solution is valid: "+str(correct)
    if not correct:
        sys.exit(1)

    #create set cover instance where universe is all flows and the collections are each edge with all of the flows on them
    return set_cover_sol, total_benefit, set_cover_benefit, list_of_benefits



