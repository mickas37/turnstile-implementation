import networkx as nx

def calc_total_number(Flows):
    total = 0
    for f_number, flow in Flows.iteritems():
        total += ((len(flow.edges()) - len(flow.nodes())) + 1)
    return total

def createSubGraphs(G, flows):
    flowMSTs = {}
    for f in flows:
        flowMSTs[f] = nx.Graph()
        for e in G.edges(data=True):
            if f in e[2]['flows']:
                flowMSTs[f].add_edge(e[0], e[1], e[2])
    return flowMSTs

def calc_benefit(G_orig, flows, e):
    G = G_orig.copy()

    #remove all edges that already have turnstiles
    for edge in G.edges(data=True):
        if G[edge[0]][edge[1]]['turnstile']==True and (edge[0],edge[1]) != (e[0],e[1]) and (edge[1],edge[0]) != (e[1],e[0]) and (edge[0],edge[1]) != (e[1],e[0]) and (edge[1],edge[0]) != (e[0],e[1]):
            G.remove_edge(edge[0],edge[1])
    Flows = createSubGraphs(G, flows)

    ben = 0
    for f_number, f in Flows.iteritems():
        if f.has_edge(e[0],e[1]):
            #print "FROM TSBENEFIT: "+str(e[0])+" "+str(e[1])+" found on flow "+str(f_number)
            fCopy = f.copy()
            fCopy.remove_edge(e[0],e[1])
            #print "AFTER "+str(e[0])+" "+str(e[1])+" was removed we have flow "+str(f_number)
            #for edge in fCopy.edges(data=True):
            #    print edge

            if nx.has_path(fCopy,e[0],e[1]):
                #print "There was a benefit"
                ben+=1
    return ben
