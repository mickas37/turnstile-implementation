"""
This file contains the implementation of the approximate solution
to the turnstile problem. The solution greedily chooses to place
a turnstile on the edge that benefits the most flows each iteration
until no more turnstiles are necessary

-S.A.M.
"""

import networkx as nx
import random
import numpy
from TSPlacementBenefit import calc_benefit, calc_total_number

def testSolution(combination, flowsArr):
    for f_number, flow in flowsArr.iteritems():
        flowCopy = flow.copy()
        for e in combination:
            if flowCopy.has_edge(*e):
                flowCopy.remove_edge(*e)
        if len(nx.cycle_basis(flowCopy)) > 0:
            return False

    return True

"""
Creates a dictionary of the different flows
"""
def createSubGraphs(G, flows):
    flowMSTs = {}
    for f in flows:
        flowMSTs[f] = nx.Graph()
        for e in G.edges(data=True):
            if f in e[2]['flows']:
                flowMSTs[f].add_edge(e[0], e[1], e[2])
    return flowMSTs

"""
calculates the weight of an edge in Graph F (corresponds to a particular flow) 
Note that e should be removed from F before F is sent into this function
  1: if there is a path from e.start -> e.end with the edge removed
  0: otherwise
"""
def calculateFlowEdgeWeight(e, F):
    #print e
    #if it is a back edge we want to give back 0 so we won't choose it
    if 0.0 in e[2]['flows'].values():
        return 0
    elif nx.has_path(F, e[0], e[1]):
        return 1
    else:
        return 0

"""
calculates the new weights of each edge in graph F (corresponds to a particular flow)
  for each edge in F:
    calculateFlowEdgeWeight(edge, F \ edge)
    add the weight to the corresponding edge in G
"""
def calculateAllFlowEdgeWeights(G, F):
    for e in F.edges(data=True):
        Fnew = F.copy()
        Fnew.remove_edge(e[0],e[1])
        F[e[0]][e[1]]['weight'] = calculateFlowEdgeWeight(e,Fnew)
        G[e[0]][e[1]]['weight'] += F[e[0]][e[1]]['weight']

"""
calculates the weights on each edge in graph G for each flow in F
"""
def calculateGraphEdgeWeights(G, Flows):
    for flow_name, F in Flows.iteritems():
        calculateAllFlowEdgeWeights(G, F)

"""
resets all edges weights in G to 0
"""
def resetEdgeWeights(G):
    for e in G.edges(data=True):
        G[e[0]][e[1]]['weight']=0

"""
while any weight on an edge is > |flows on that edge|:
  placeTurnstile(G)
  Reset edge weights to 0 on G
  calculateGraphEdgeWeights(G, Flows)
return G
"""
def approx(G, flow_names):
    Flows = createSubGraphs(G, flow_names)
    resetEdgeWeights(G)
    #the weight on each edge is the sum of paths from the start to end node on each flow on that edge
    calculateGraphEdgeWeights(G, Flows)
    # print "G with initial weights"
    
    # for e in G.edges(data=True):
    #    print e    
    total_benefit = calc_total_number(Flows)
    approx_benefit = 0
    actual_benefit = 0
    list_of_benefits = []
    prev_benefit = 0
    done=False
    # for e in G.edges(data=True):
    #     print e
    while not done:
        largest_weight = 0
        largest_weight_edge = ()
        for e in G.edges():
            if G[e[0]][e[1]]['weight'] >= largest_weight:
                largest_weight=G[e[0]][e[1]]['weight']
                largest_weight_edge=e
        if len(G[largest_weight_edge[0]][largest_weight_edge[1]]['flows']) == 0:
            done=True
            continue

        G[largest_weight_edge[0]][largest_weight_edge[1]]['turnstile']=True
        ben_there = calc_benefit(G,flow_names,largest_weight_edge)
        list_of_benefits.append(ben_there+prev_benefit)
        prev_benefit = prev_benefit+ben_there
        approx_benefit += largest_weight
        actual_benefit += ben_there

        #place a turnstile on the largest weighted edge and remove it from the flows
        for f in G[largest_weight_edge[0]][largest_weight_edge[1]]['flows']:
            if Flows[f].has_edge(largest_weight_edge[0], largest_weight_edge[1]):
                Flows[f].remove_edge(largest_weight_edge[0], largest_weight_edge[1])
            else:
                Flows[f].remove_edge(largest_weight_edge[1], largest_weight_edge[0])


        resetEdgeWeights(G)

        #recalculate edge weights for the flows and graph
        calculateGraphEdgeWeights(G, Flows)

        done=True
        for e in G.edges(data=True):
            if G[e[0]][e[1]]['weight'] > 0:
                done=False
    number_of_turnstiles = 0
    sol_set = []
    for e in G.edges(data=True):
        if e[2]['turnstile'] == True:
            sol_set.append((e[0],e[1]))
            number_of_turnstiles += 1

    print "Approx solution is valid : "+str(testSolution(sol_set, Flows))
    if not testSolution(sol_set, Flows):
        print "Approx did not provide a correct solution: "
        print str(sol_set)
        for e in G.edges(data=True):
            print str(e)
    return number_of_turnstiles, total_benefit, actual_benefit, list_of_benefits
